<?php

namespace Smorken\Auth\Proxy\Proxies\Contracts;

interface Hasher
{

    /**
     * @param  string  $string
     * @return string
     */
    public function get(string $string): string;
}
