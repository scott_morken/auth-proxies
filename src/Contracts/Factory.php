<?php

namespace Smorken\Auth\Proxy\Proxies\Contracts;

interface Factory
{
    /**
     * @param  string  $key
     * @param  null  $default
     * @return mixed
     */
    public function fromConfig(string $key, mixed $default = null): mixed;

    /**
     * @param  string  $proxy_name
     * @param  array  $config
     * @return \Smorken\Auth\Proxy\Proxies\Contracts\Proxy
     */
    public function make(string $proxy_name, array $config = []): Proxy;
}
