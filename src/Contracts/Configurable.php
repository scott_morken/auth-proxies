<?php

namespace Smorken\Auth\Proxy\Proxies\Contracts;

interface Configurable
{

    /**
     * @param mixed ...$params
     * @return void
     */
    public function configure(...$params): void;
}
