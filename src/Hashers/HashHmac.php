<?php

namespace Smorken\Auth\Proxy\Proxies\Hashers;

use Smorken\Auth\Proxy\Proxies\Contracts\Hasher;

class HashHmac implements Hasher
{
    protected string $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    /**
     * @param  string  $string
     * @return string
     */
    public function get(string $string): string
    {
        if (!$this->secret) {
            throw new \InvalidArgumentException("Secret is required.");
        }
        return hash_hmac('sha256', $string, $this->secret);
    }
}
