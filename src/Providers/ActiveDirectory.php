<?php

namespace Smorken\Auth\Proxy\Proxies\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use LdapQuery\Contracts\Filter;
use LdapQuery\LdapQuery;
use LdapQuery\Query\Builders\QueryBuilder;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Models\User;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\InvalidException;
use Smorken\Auth\Proxy\Proxies\Contracts\Hasher;

class ActiveDirectory extends Base
{

    /**
     * @var \Smorken\Auth\Proxy\Proxies\Contracts\Hasher
     */
    protected Hasher $hasher;

    protected array $map = [
        'id' => 'uid',
        'username' => 'samaccountname',
        'first_name' => 'givenname',
        'last_name' => 'sn',
        'email' => 'mail',
    ];

    /**
     * ActiveDirectory constructor.
     *
     * @param  \LdapQuery\LdapQuery  $backend
     * @param  \Smorken\Auth\Proxy\Proxies\Contracts\Hasher  $hasher
     */
    public function __construct(LdapQuery $backend, Hasher $hasher)
    {
        $this->setBackend($backend);
        $this->hasher = $hasher;
    }

    /**
     * @param  string  $username
     * @param  string  $password
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function authenticate(string $username, string $password): Response
    {
        $key = $this->getAuthKey($username, $password);
        if ($this->shouldCache()) {
            $cached = $this->getCached($key, $username);
            if ($cached) {
                return $cached;
            }
        }
        $response = $this->doAuthentication($username, $password);
        $this->setCached($response, $key);
        return $response;
    }

    /**
     * @param  mixed  $proxy_data
     * @return User
     */
    public function convertToUser(mixed $proxy_data): User
    {
        $map = $this->map;
        $data = [];
        foreach ($map as $user_attr => $ldap_attr) {
            $ldap_attr = strtolower($ldap_attr);
            try {
                $value = $proxy_data?->$ldap_attr;
                if ($user_attr === 'username') {
                    $value = strtolower($value ?? '');
                }
                $data[$user_attr] = $value;
            } catch (\Throwable) {
                $data[$user_attr] = null;
            }
        }
        $this->addGroups($proxy_data, $data);
        return $this->getUserModel()
                    ->newInstance($data);
    }

    /**
     * @param  array  $criteria
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function search(array $criteria): Response
    {
        $response = $this->getResponseModel();
        $filter = $this->getFilterArray($criteria);
        if ($filter) {
            $query = $this->createUserQuery();
            foreach ($filter as $f) {
                $query->where($f[0], $this->getBackend()
                                          ->escape($f[1]), $f[2]);
            }
            $entries = $this->getBackend()
                            ->execute($query)
                            ->get();
            return $response->fromUsers($this->convertToUserCollection($entries));
        }
        return $response->fromUsers(new Collection());
    }

    protected function addGroups(mixed $proxy_data, array &$data): void
    {
        $group_attr = strtolower($this->getConfigItem('group_attribute'));
        if ($group_attr && $this->shouldAddData()) {
            $groups = [];
            $proxy_groups = $proxy_data->$group_attr;
            if (is_array($proxy_groups)) {
                foreach ($proxy_groups as $group) {
                    $groups[] = $this->getGroupName($group);
                }
            }
            $data['data'] = ['groups' => $groups];
        }
    }

    protected function convertToUserCollection(iterable $entries): Collection
    {
        $coll = new Collection();
        foreach ($entries as $user) {
            $coll->push($this->convertToUser($user));
        }
        return $coll;
    }

    protected function createUserQuery(): QueryBuilder
    {
        $filters = $this->getBackend()
                        ->getBackend()
                        ->getOptionByKey('filters', []);
        $pf = Arr::get($filters, 'person', []);
        $q = $this->getBackend()
                  ->newQuery()
                  ->select([]);
        if (Arr::get($pf, 'category', false)) {
            $q->where(Arr::get($pf, 'category'), $this->getBackend()
                                                      ->escape(Arr::get($pf, 'category_type', 'person')));
        }
        if (Arr::get($pf, 'samaccounttype', false)) {
            $q->where('samaccounttype', $this->getBackend()
                                             ->escape(Arr::get($pf, 'samaccounttype')));
        }
        return $q;
    }

    protected function doAuthentication(string $username, string $password): Response
    {
        $r = $this->getResponseModel();
        try {
            $this->validateAuthentication($username, $password);

            $auth = $this->getBackend()
                         ->authenticate($username, $password);
            if ($auth) {
                $ldapuser = $this->getBackend()
                                 ->findUser($username);
                $response = $r->fromUser($this->convertToUser($ldapuser), $auth);
            } else {
                $response = $r->fromException(new InvalidException("Unable to authenticate user $username."));
            }
        } catch (\Exception $e) {
            if (!$e instanceof AuthenticationException) {
                $this->handleException($e);
            }
            $response = $r->fromException($e);
        }
        return $response;
    }

    protected function getAuthKey(string $username, string $password): string
    {
        return $this->hasher->get(sprintf('%s%s%s%s', get_called_class(), $username, $password,
            $this->shouldAddData() ? 1 : 0));
    }

    protected function getFilterArray(array $criteria): array
    {
        $filter = [];
        $operators = $this->getOperators();
        foreach ($criteria as $field => $value) {
            $proxy_attr = Arr::get($this->map, $field);
            if ($proxy_attr && strlen($value)) {
                $filter[] = [$proxy_attr, $value, Arr::get($operators, $proxy_attr, Filter::EQUALS_FILTER)];
            }
        }
        return $filter;
    }

    protected function getGroupName(string $group): string
    {
        $expDn = explode(',', $group);
        $first = reset($expDn);
        if (strtolower(substr($first, 0, 3)) == 'cn=') {
            return substr($first, 3);
        }
        return $group;
    }

    protected function getOperators(): array
    {
        $types = [
            'id' => Filter::EQUALS_FILTER,
            'username' => Filter::EQUALS_FILTER,
            'first_name' => Filter::STARTS_WITH_FILTER,
            'last_name' => Filter::STARTS_WITH_FILTER,
            'email' => Filter::STARTS_WITH_FILTER,
        ];
        $operators = [];
        $map = $this->map;
        foreach ($types as $user_attr => $filter_type) {
            $proxy_attr = Arr::get($map, $user_attr);
            if ($proxy_attr) {
                $operators[$proxy_attr] = $filter_type;
            }
        }
        return $operators;
    }
}
