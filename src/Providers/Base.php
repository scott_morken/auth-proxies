<?php

namespace Smorken\Auth\Proxy\Proxies\Providers;

use Illuminate\Contracts\Cache\Factory;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Arr;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Models\User;
use Smorken\Auth\Proxy\Common\Exceptions\InvalidException;
use Smorken\Auth\Proxy\Common\Traits\Log;
use Smorken\Auth\Proxy\Proxies\Contracts\Proxy;

abstract class Base implements Proxy
{

    use Log;

    /**
     * @var mixed
     */
    protected mixed $backend;

    /**
     * @var Factory|null
     */
    protected ?Factory $cache = null;

    /**
     * @var array
     */
    protected array $config = [];

    /**
     * @var array
     */
    protected array $default_config = [
        'cache_ttl' => 60,
        'models' => [
            'response' => \Smorken\Auth\Proxy\Common\Models\Response::class,
            'user' => \Smorken\Auth\Proxy\Common\Models\User::class,
        ],
    ];

    /**
     * @var ExceptionHandler|null
     */
    protected ?ExceptionHandler $exceptionHandler = null;

    protected bool $extended_data = false;

    protected array $map = [];

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Models\Response|null
     */
    protected ?Response $response = null;

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Models\User|null
     */
    protected ?User $user = null;

    public function addData(bool $should_add): void
    {
        $this->extended_data = $should_add;
    }

    public function configure(...$params): void
    {
        $c = $params[0];
        $this->setConfig($c);
        $usercls = $this->getConfigItem('models.user', \Smorken\Auth\Proxy\Common\Models\User::class);
        $this->user = new $usercls;
        $responsecls = $this->getConfigItem('models.response', \Smorken\Auth\Proxy\Common\Models\Response::class);
        $this->response = new $responsecls;
        $this->map = array_merge($this->map, $this->getConfigItem('map', []));
        $this->debug = $this->getConfigItem('debug', false);
    }

    /**
     * @return mixed
     */
    public function getBackend(): mixed
    {
        return $this->backend;
    }

    /**
     * @param  mixed  $backend
     * @return void
     */
    public function setBackend(mixed $backend): void
    {
        $this->backend = $backend;
    }

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function getResponseModel(): Response
    {
        return $this->response;
    }

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\User
     */
    public function getUserModel(): User
    {
        return $this->user;
    }

    /**
     * @return \Illuminate\Contracts\Cache\Factory
     */
    protected function getCache(): Factory
    {
        if (!$this->cache && function_exists('app')) {
            $this->cache = app(Factory::class);
        }
        return $this->cache;
    }

    /**
     * @param  \Illuminate\Contracts\Cache\Factory  $cache
     */
    public function setCache(Factory $cache): void
    {
        $this->cache = $cache;
    }

    /**
     * @param  string  $key
     * @param  string  $username
     * @return mixed
     */
    protected function getCached(string $key, string $username): mixed
    {
        if ($this->shouldCache()) {
            $cached = $this->getCache()
                           ->get($key);
            if ($cached && $cached->isAuthenticated()) {
                $user = $cached->user;
                if ($user && $user->username === $username) {
                    return $cached;
                }
            }
        }
        return null;
    }

    /**
     * @param  string  $key
     * @param  null  $default
     * @return mixed
     */
    protected function getConfigItem(string $key, mixed $default = null): mixed
    {
        return Arr::get($this->config, $key, $default);
    }

    protected function getExceptionHandler(): ExceptionHandler
    {
        if (!$this->exceptionHandler && function_exists('app')) {
            $this->exceptionHandler = app(ExceptionHandler::class);
        }
        return $this->exceptionHandler;
    }

    public function setExceptionHandler(ExceptionHandler $handler): void
    {
        $this->exceptionHandler = $handler;
    }

    protected function handleException(\Throwable $e): void
    {
        $eh = $this->getExceptionHandler();
        if ($eh) {
            $eh->report($e);
        }
    }

    protected function setCached(\Smorken\Auth\Proxy\Common\Contracts\Models\Response $response, string $key): void
    {
        if ($this->shouldCache() && $response->isAuthenticated()) {
            $this->getCache()
                 ->set($key, $response, $this->getConfigItem('cache_ttl'));
        }
    }

    /**
     * Merge proxy config with general config
     *
     * @param  array  $config
     */
    protected function setConfig(array $config): void
    {
        $this->config = $this->default_config;
        $proxy_part = Arr::get($config, sprintf('proxies.%s', get_called_class()), []);
        unset($config['proxies']);
        $this->config = array_replace_recursive($config, $proxy_part);
    }

    protected function shouldAddData(): bool
    {
        return $this->extended_data;
    }

    protected function shouldCache(): bool
    {
        return $this->getCache() && $this->getConfigItem('cache_ttl');
    }

    protected function validateAuthentication($username, $password): void
    {
        if (!$username || !$password) {
            throw new InvalidException('Missing username and/or password.');
        }
    }
}
