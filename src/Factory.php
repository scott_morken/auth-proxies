<?php

namespace Smorken\Auth\Proxy\Proxies;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use Smorken\Auth\Proxy\Proxies\Contracts\Proxy;

class Factory implements \Smorken\Auth\Proxy\Proxies\Contracts\Factory
{
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected Application $application;

    protected array $config = [];

    protected array $proxies = [];

    public function __construct(Application $application, array $config)
    {
        $this->application = $application;
        $this->config = $config;
    }

    public function fromConfig(string $key, mixed $default = null): mixed
    {
        return Arr::get($this->config, $key, $default);
    }

    public function make(string $proxy_name, array $config = []): Proxy
    {
        $proxy = Arr::get($this->proxies, $proxy_name);
        if (!$proxy) {
            if (array_key_exists($proxy_name, Arr::get($this->config, 'proxies', []))) {
                $proxy = $this->application->make($proxy_name);
                $this->proxies[$proxy_name] = $proxy;
            } else {
                throw new \InvalidArgumentException("$proxy_name is not a valid proxy.");
            }
        }
        $config = array_replace_recursive($this->config, $config);
        $proxy->configure($config);
        return $proxy;
    }
}
