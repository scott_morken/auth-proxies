<?php

namespace Tests\Smorken\Auth\Proxy\Proxies\Unit;

use Illuminate\Contracts\Foundation\Application;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory;

class FactoryTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testMakeCreatesProxy()
    {
        list($sut, $app) = $this->getSut();
        $ad = m::mock(ActiveDirectory::class);
        $app->shouldReceive('make')
            ->once()
            ->with(ActiveDirectory::class)
            ->andReturn($ad);
        $ad->shouldReceive('configure')
           ->once()
           ->with(m::type('array'));
        $p = $sut->make(ActiveDirectory::class);
        $this->assertSame($ad, $p);
    }

    public function testMakeThrowsExceptionWhenProxyNotKnown()
    {
        list($sut, $app) = $this->getSut();
        $app->shouldReceive('make')
            ->never()
            ->with('FooBar');
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("FooBar is not a valid proxy.");
        $p = $sut->make('FooBar');
    }

    protected function getDefaultConfig()
    {
        return [
            'cache_ttl' => 60,
            'models'    => [
                'response' => \Smorken\Auth\Proxy\Common\Models\Response::class,
                'user'     => \Smorken\Auth\Proxy\Common\Models\User::class,
            ],
            'proxies'   => [
                ActiveDirectory::class => [
                    'group_attribute' => false,
                    'map'             => [
                        'id'         => 'uid',
                        'username'   => 'samaccountname',
                        'first_name' => 'givenname',
                        'last_name'  => 'sn',
                        'email'      => 'mail',
                    ],
                    'backend_options' => [],
                ],
            ],
        ];
    }

    protected function getSut($config = [])
    {
        $config = array_replace_recursive($this->getDefaultConfig(), $config);
        $app = m::mock(Application::class);
        $sut = new \Smorken\Auth\Proxy\Proxies\Factory($app, $config);
        return [$sut, $app];
    }
}
