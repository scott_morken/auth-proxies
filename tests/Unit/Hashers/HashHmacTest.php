<?php

namespace Tests\Smorken\Auth\Proxy\Proxies\Unit\Hashers;

use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Proxies\Hashers\HashHmac;

class HashHmacTest extends TestCase
{

    public function testHashableGet(): void
    {
        $sut = new HashHmac('foo');
        $this->assertEquals('f9320baf0249169e73850cd6156ded0106e2bb6ad8cab01b7bbbebe6d1065317', $sut->get('bar'));
    }

    public function testNoSecretIsException(): void
    {
        $sut = new HashHmac('');
        $this->expectException(\InvalidArgumentException::class);
        $sut->get('foo');
    }
}
