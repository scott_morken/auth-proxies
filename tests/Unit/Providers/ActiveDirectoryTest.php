<?php

namespace Tests\Smorken\Auth\Proxy\Proxies\Unit\Providers;

use Illuminate\Contracts\Cache\Factory;
use Illuminate\Contracts\Debug\ExceptionHandler;
use LdapQuery\Contracts\Client;
use LdapQuery\LdapQuery;
use LdapQuery\Models\VO\Model;
use LdapQuery\Query\Builders\QueryBuilder;
use LdapQuery\Results\Ldap;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;
use Smorken\Auth\Proxy\Common\Models\User;
use Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory;

class ActiveDirectoryTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testAuthenticateCached(): void
    {
        [$sut, $backend, $hasher, $cache] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $user = new User(['id' => 1, 'username' => 'foo']);
        $resp = (new \Smorken\Auth\Proxy\Common\Models\Response())->fromUser($user, true);
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn($resp);
        $r = $sut->authenticate('foo', '123');
        $this->assertSame($resp, $r);
    }

    public function testAuthenticateCachedVerifiesAuthenticated(): void
    {
        [$sut, $backend, $hasher, $cache] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $user = new User(['id' => 1, 'username' => 'foo']);
        $resp = (new \Smorken\Auth\Proxy\Common\Models\Response())->fromUser($user, false);
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn($resp);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(false);
        $r = $sut->authenticate('foo', '123');
        $this->assertFalse($r->isAuthenticated());
    }

    public function testAuthenticateCachedVerifiesUsernameMatch(): void
    {
        [$sut, $backend, $hasher, $cache] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $user = new User(['id' => 1, 'username' => 'bar']);
        $resp = (new \Smorken\Auth\Proxy\Common\Models\Response())->fromUser($user, false);
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn($resp);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(false);
        $r = $sut->authenticate('foo', '123');
        $this->assertFalse($r->isAuthenticated());
    }

    public function testAuthenticateNoCachingCanAuthenticateCanFindUser(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut(['cache_ttl' => false]);
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->never();
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(true);
        $proxydata = new Model([
            'uid' => 1,
            'samaccountname' => 'foo',
            'givenname' => 'Joe',
            'sn' => 'Bobber',
            'mail' => 'joe@foo.com',
        ]);
        $backend->shouldReceive('findUser')
                ->once()
                ->with('foo')
                ->andReturn($proxydata);
        $cache->shouldReceive('set')
              ->never();
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 200,
            'error' => false,
            'authenticated' => true,
            'message' => null,
            'user' => [
                'id' => 1,
                'username' => 'foo',
                'first_name' => 'Joe',
                'last_name' => 'Bobber',
                'email' => 'joe@foo.com',
                'data' => [],
            ],
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedCanAuthenticateCanFindUser(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(true);
        $proxydata = new Model([
            'uid' => 1,
            'samaccountname' => 'foo',
            'givenname' => 'Joe',
            'sn' => 'Bobber',
            'mail' => 'joe@foo.com',
        ]);
        $backend->shouldReceive('findUser')
                ->once()
                ->with('foo')
                ->andReturn($proxydata);
        $cache->shouldReceive('set')
              ->once()
              ->with('hash_foo123', m::type(Response::class), 60);
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 200,
            'error' => false,
            'authenticated' => true,
            'message' => null,
            'user' => [
                'id' => 1,
                'username' => 'foo',
                'first_name' => 'Joe',
                'last_name' => 'Bobber',
                'email' => 'joe@foo.com',
                'data' => [],
            ],
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedCanAuthenticateCanFindUserWithExtendedData(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1231')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(true);
        $proxydata = new Model([
            'uid' => 1,
            'samaccountname' => 'foo',
            'givenname' => 'Joe',
            'sn' => 'Bobber',
            'mail' => 'joe@foo.com',
            'memberOf' => [
                'cn=foobar,ou=blah',
                'fizbuz',
            ]
        ]);
        $backend->shouldReceive('findUser')
                ->once()
                ->with('foo')
                ->andReturn($proxydata);
        $cache->shouldReceive('set')
              ->once()
              ->with('hash_foo123', m::type(Response::class), 60);
        $sut->addData(true);
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 200,
            'error' => false,
            'authenticated' => true,
            'message' => null,
            'user' => [
                'id' => 1,
                'username' => 'foo',
                'first_name' => 'Joe',
                'last_name' => 'Bobber',
                'email' => 'joe@foo.com',
                'data' => [
                    'groups' => [
                        'foobar',
                        'fizbuz',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedCanAuthenticateCantFindUser(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(true);
        $backend->shouldReceive('findUser')
                ->once()
                ->with('foo')
                ->andReturn(null);
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 401,
            'error' => true,
            'authenticated' => false,
            'message' => 'Incomplete user data.',
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedCantAuthenticate(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andReturn(false);
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 401,
            'error' => true,
            'authenticated' => false,
            'message' => 'Invalid username and/or password.',
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedWithExceptionHasDisplay(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andThrow(new SystemException('Fail!!'));
        $log->shouldReceive('report')
            ->once()
            ->with(m::type(SystemException::class));
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 500,
            'error' => true,
            'authenticated' => false,
            'message' => 'There was an unhandled error.  Please try your request again later.',
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedWithExceptionNoDisplay(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andThrow(new \InvalidArgumentException('Some other exception.'));
        $log->shouldReceive('report')
            ->once()
            ->with(m::type(\InvalidArgumentException::class));
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 500,
            'error' => true,
            'authenticated' => false,
            'message' => 'There was an error connecting to the authentication provider.',
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testAuthenticateNotCachedWithExceptionOverrideDisplay(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        $hasher->shouldReceive('get')
               ->once()
               ->with('Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectoryfoo1230')
               ->andReturn('hash_foo123');
        $cache->shouldReceive('get')
              ->once()
              ->with('hash_foo123')
              ->andReturn(false);
        $backend->shouldReceive('authenticate')
                ->once()
                ->with('foo', '123')
                ->andThrow(new SystemException('Fail!!', 'Some other message.'));
        $log->shouldReceive('report')
            ->once()
            ->with(m::type(SystemException::class));
        $r = $sut->authenticate('foo', '123');
        $expected = [
            'status' => 500,
            'error' => true,
            'authenticated' => false,
            'message' => 'Some other message.',
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testSearchWithExtendedData(): void
    {
        [$sut, $backend, $hasher, $cache, $log] = $this->getSut();
        define('LDAP_DEREF_NEVER', 1);
        $client = m::mock(Client::class);
        $ad = new \LdapQuery\Backends\ActiveDirectory($client, new Ldap(new Model()));
        $backend = new LdapQuery($ad,
            new QueryBuilder(new \LdapQuery\Query\Filters\ActiveDirectory()));
        $sut->setBackend($backend);
        $entries = [
            'count' => 2,
            0 => [
                'sn' => [
                    'count' => 1,
                    0 => 'Bobber',
                ],
                0 => 'sn',
                'objectclass' => [
                    'count' => 4,
                    0 => 'inetOrgPerson',
                    1 => 'organizationalPerson',
                    2 => 'person',
                    3 => 'top',
                ],
                1 => 'objectclass',
                'uid' => [
                    'count' => 1,
                    0 => 'foo',
                ],
                2 => 'uid',
                'mail' => [
                    'count' => 1,
                    0 => 'joe@foo.com',
                ],
                3 => 'mail',
                'cn' => [
                    'count' => 1,
                    0 => 'Joe Bobber',
                ],
                4 => 'cn',
                'count' => 5,
                'dn' => 'uid=foo,dc=domain,dc=org',
            ],
        ];
        $client->shouldReceive('escape')
               ->andReturnUsing(function ($args) {
                   return $args;
               });
        $client->shouldReceive('isConnected')
               ->andReturn(true);
        $client->shouldReceive('isBound')
               ->andReturn(true);
        $client->shouldReceive('search')
               ->once()
               ->with('dc=domain,dc=org', '(&(objectCategory=person)(samaccounttype=805306368)(sn=Bob*))', [])
               ->andReturn($entries);
        $client->shouldReceive('getEntries')
               ->once()
               ->andReturn($entries);
        $sut->addData(true);
        $r = $sut->search(['last_name' => 'Bob']);
        $expected = [
            'status' => 200,
            'error' => false,
            'authenticated' => false,
            'message' => null,
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $r->$k);
        }
        $this->assertCount(1, $r->users);
        $expectedUser = [
            'id' => 'foo',
            'username' => '',
            'first_name' => null,
            'last_name' => 'Bobber',
            'email' => 'joe@foo.com',
            'data' => [
                'groups' => [],
            ],
        ];
        $this->assertEquals($expectedUser, $r->users[0]->toArray());
    }

    protected function getDefaultConfig(): array
    {
        return [
            'cache_ttl' => 60,
            'models' => [
                'response' => \Smorken\Auth\Proxy\Common\Models\Response::class,
                'user' => \Smorken\Auth\Proxy\Common\Models\User::class,
            ],
            'proxies' => [
                ActiveDirectory::class => [
                    'group_attribute' => 'memberOf',
                    'map' => [
                        'id' => 'uid',
                        'username' => 'samaccountname',
                        'first_name' => 'givenname',
                        'last_name' => 'sn',
                        'email' => 'mail',
                    ],
                ],
            ],
        ];
    }

    protected function getSut($config = []): array
    {
        $config = array_replace_recursive($this->getDefaultConfig(), $config);
        $backend = m::mock(LdapQuery::class);
        $hasher = m::mock(\Smorken\Auth\Proxy\Proxies\Contracts\Hasher::class);
        $cache = m::mock(Factory::class);
        $except_handler = m::mock(ExceptionHandler::class);
        $sut = new ActiveDirectory($backend, $hasher);
        $sut->setCache($cache);
        $sut->setExceptionHandler($except_handler);
        $sut->configure($config);
        return [$sut, $backend, $hasher, $cache, $except_handler];
    }
}
