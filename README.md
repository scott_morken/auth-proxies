## Auth Proxies library

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+
* [Composer](https://getcomposer.org/)

#### Installation

Add to your composer.json

```
"require": {
    "smorken/auth-proxies": "^6.0"
}
```
`$ composer update`

Config (`config/auth-proxy.php`)
```php
<?php
return [
    'debug'       => env('APP_DEBUG', false),
        'cache_ttl'   => 60 * 60,
        'controllers' => [
            'login' => \Smorken\Auth\Proxy\Http\Controllers\Login\Controller::class,
            'admin' => \Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class,
        ],
        'models'      => [
            'response' => \Smorken\Auth\Proxy\Common\Models\Response::class,
            'user'     => \Smorken\Auth\Proxy\Common\Models\User::class,
        ],
        'proxies'     => [
            \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory::class => [
                'group_attribute' => env('AUTHPROXY_GROUP_ATTR', 'memberof'),
                'map'             => [
                    'id'         => env('AUTHPROXY_USER_ID', 'uid'),
                    'username'   => env('AUTHPROXY_USER_USERNAME', 'samaccountname'),
                    'first_name' => env('AUTHPROXY_USER_FIRSTNAME', 'givenname'),
                    'last_name'  => env('AUTHPROXY_USER_LASTNAME', 'sn'),
                    'email'      => env('AUTHPROXY_USER_EMAIL', 'mail'),
                ],
                'backend_options' => [
                    'bind_filter'    => env('LDAP_ACCOUNT_SUFFIX', '%s@domain.edu'),
                    'host'           => env('LDAP_DOMAIN_CONTROLLER', 'ldap.domain.edu'),
                    'base_dn'        => env('LDAP_BASE_DN', 'ou=group,dc=domain,dc=org'),
                    'bind_user'      => env('LDAP_ADMIN_USERNAME', ''),
                    'bind_password'  => env('LDAP_ADMIN_PASSWORD', ''),
                    'client_options' => [
                        'start_tls' => env('LDAP_START_TLS', false),
                        'ssl'       => env('LDAP_SSL', true),
                    ],
                ],
            ],
        ],
        'provider'    => [
            'endpoints'       => [
                'authenticate' => env('AUTHPROXY_PROVIDER_AUTH_ENDPOINT', 'http://localhost/authenticate'),
                'search'       => env('AUTHPROXY_PROVIDER_SEARCH_ENDPOINT', 'http://localhost/search'),
            ],
            'token'           => env('AUTHPROXY_PROVIDER_TOKEN'),
            'host'            => env('AUTHPROXY_PROVIDER_HOST'),
            'data'            => env('AUTHPROXY_PROVIDER_DATA', 0),
            'backend_options' => [
                'http_errors'     => false,
                'connect_timeout' => 5,
                //'verify'        => false,
            ],
        ],
];
```

ActiveDirectory
```php
<?php
$config = config('auth-proxy', []);
$backend_opts = \Illuminate\Support\Arr::get($config, sprintf('proxies.%s.backend_options', \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory::class), []);
$backend = new \LdapQuery\Opinionated\ActiveDirectory($backend_opts);
$hasher = new \Smorken\Auth\Proxy\Proxies\Hashers\HashHmac('secret key');
$provider = new \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory($backend->getLdapQuery(), $hasher);
```
